<?php

require_once('notify.inc');

$rootdir = $argv[1];
$tradename = $argv[2];
$txid = $argv[3];
$bootstrapped = FALSE;

/**
 * Print a usage message and exit immediately if anything went wrong.
 */
function _usage($message) {
  global $argv, $bootstrapped;
  if ($bootstrapped) {
    // TODO watchdog
  }
  // TODO print message
  print 'ERROR: ' . $message;
  print "Usage: $argv[0] [DRUPAL ROOT] [COIN TRADENAME] [TXID]";
  exit(1);
}

// Drupal bootstrap.
$result = _bootstrap($rootdir);
if ($result != _BOOTSTRAP_OK) {
  _usage($result);
}
$bootstrapped = TRUE;

// Preflight checks.
if (empty($tradename)) {
  _usage('No coin tradename argument was supplied.');
}
if (empty($txid)) {
  _usage('No transaction ID argument was supplied.');
}



// Insert notification into queue.
$fields = array(
  'txid' => $txid,
  'coin_tradename' => $tradename,
  'notification_time' => time(),
);
db_insert('cryptocoin_transaction_walletnotify')->fields($fields)->execute();


