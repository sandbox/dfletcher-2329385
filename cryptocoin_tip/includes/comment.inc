<?php

/**
 * Implements hook_cryptocoin_tip_receiver_type_info() on behalf of comment.
 */
function comment_cryptocoin_tip_receiver_type_info($field, $instance) {
  $receiver_type_info = array();
  if ($instance['entity_type'] == 'comment') {
    $receiver_type_info['comment'] = array(
      'label' => t('Comment'),
      'uid callback' => 'comment_cryptocoin_tip_uid',
      'uid arguments' => array(),
    );
  }
  return $receiver_type_info;
}

/**
 * 'uid callback' for the comment receiver type.
 */
function comment_cryptocoin_tip_uid($form, $form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $node = $form['#node'];
  $comment = $form['#entity'];
  if (isset($comment->pid)) {
    $parent_comment = comment_load($comment->pid);
    return $parent_comment->uid;
  }
  else {
    return $node->uid;
  }
}
