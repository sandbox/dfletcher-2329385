<?php

/**
 * Implements hook_cryptocoin_tip_receiver_type_info() on behalf of entityreference.
 */
function entityreference_cryptocoin_tip_receiver_type_info($field, $instance) {
  $receiver_type_info = array();
  $fields_info = field_info_instances($instance['entity_type'], $instance['bundle']);
  $referencefields = array();
  foreach ($fields_info as $fieldname => &$field_info) {
    if ($field_info['widget']['module'] == 'entityreference') {
      $receiver_type_info['entitytype:' . $fieldname] = array(
        'label' => t('Entity reference: !entity', array('!entity' => $field_info['label'])),
        'uid callback' => 'entityreference_cryptocoin_tip_uid',
        'uid arguments' => array($fieldname),
      );
    }
  }
  return $receiver_type_info;
}

/**
 * 'uid callback' for the entityreference receiver type.
 */
function entityreference_cryptocoin_tip_uid($form, $form_state, $field, $instance, $langcode, $items, $delta, $element, $fieldname) {
  if (!isset($form_state['field'][$fieldname][LANGUAGE_NONE]['field']['settings']['target_type'])) {
    return 0;
  }
  $entity_type = $form_state['field'][$fieldname][LANGUAGE_NONE]['field']['settings']['target_type'];
  // NOTE: reading the value from $_REQUEST isn't so hot but this value does
  // not seem to be available anywhere else.
  $entity_id = isset($form_state['values'][$fieldname]) ? $form_state['values'][$fieldname] : (isset($_REQUEST[$fieldname]) ? intval($_REQUEST[$fieldname]) : 0);
  if (!$entity_id || !$entity_type) {
    return 0;
  }
  $entities = entity_load($entity_type, array($entity_id));
  if (empty($entities)) {
    return 0;
  }
  $entity = reset($entities);
  if (empty($entity) || !isset($entity->uid)) {
    return 0;
  }
  return $entity->uid;  
}
