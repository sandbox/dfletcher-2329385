<?php

/**
 * Bootstrap and common api for the notification scripts.
 */

/**
 * Do not allow the notification system to execute via HTTP request.
 */
if (isset($_SERVER['REMOTE_ADDR'])) {
  print "This script must not be executed from a web server.";
  exit(2);
}

define('_BOOTSTRAP_OK', '++OK');
 
function _bootstrap($rootdir) {
  if (empty($rootdir) || !is_dir($rootdir)) {
    return '"' . $rootdir . '" is not a directory.';
  }
  define('DRUPAL_ROOT', $rootdir);
  require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
  if (!function_exists('drupal_bootstrap')) {
    return 'Required function drupal_bootstrap() was not found. Is the Drupal root directory set correctly?';
  }
  drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
  return _BOOTSTRAP_OK;
}
