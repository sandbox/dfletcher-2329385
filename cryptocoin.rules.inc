<?php

/**
 * Implements hook_rules_data_info().
 */
function cryptocoin_rules_data_info() {
  return array(
    'cryptocoin' => array(
      'label' => t('cryptocoin'),
      'parent' => 'entity',
      'group' => t('CryptoCoin'),
    ),
  );
}

