(function ($) {

  var AUTODATA_REFRESH_INTERVAL = 10.0;
  var AUTODATA_NOTIFICATION_DELAY = 2.5;

  var _autodata_requests = {};

  function autodata_notify($e, updateclass) {
    var $notify = $e.closest('.autodata-notification-wrapper').addClass(updateclass);
    window.setTimeout(function() {
      $notify.removeClass(updateclass);
    }, AUTODATA_NOTIFICATION_DELAY * 1000.0);
  }

  function autodata_request(url, $element) {
    if (!(url in _autodata_requests)) {
      _autodata_requests[url] = [];
      $.ajax({
        dataType: 'json',
        url: url,
        success: function(data) {
          for (var i in _autodata_requests[url]) {
            var $e = _autodata_requests[url][i];
            var oldval = $e.html();
            if (data.data != oldval) {
              $e.html(data.data);
              var updateclass = 'autodata-updated';
              if (!isNaN(data.data) && !isNaN(oldval)) {
                updateclass = updateclass + ' ' + ((parseFloat(oldval) > parseFloat(data.data)) ? 'autodata-value-down' : 'autodata-value-up');
              }
              autodata_notify($e, updateclass);
            }
          }
          delete _autodata_requests[url];
        },
        error: function(data) {
          delete _autodata_requests[url];
        }
      });
    }
    _autodata_requests[url][_autodata_requests[url].length] = $element;
  }

  $(document).ready(function() {
    window.setInterval(function() {
      $('.autodata').each(function() {
        var $element = $(this);
        autodata_request($element.data('auto'), $element);
      });
    }, AUTODATA_REFRESH_INTERVAL * 1000.0);
  });

}(jQuery));
