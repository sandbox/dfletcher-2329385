<?php

/**
 * Implements hook_rules_event_info().
 */
function cryptocoin_transaction_rules_event_info() {
  $items = array(
    'cryptocoin_transaction_raw' => array(
      'label' => t('Any coin transaction'),
      'group' => t('Cryptocoin Transaction'),
      'variables' => rules_events_node_variables(t('created content')), //FIXME
    ),
    'cryptocoin_transaction_local' => array(
      'label' => t('Local account coin transaction'),
      'group' => t('Cryptocoin Transaction'),
      'variables' => rules_events_node_variables(t('updated content'), TRUE), //FIXME
    ),
  );
  return $items;
}

/**
 * Implements hook_rules_data_info().
 */
function cryptocoin_transaction_rules_data_info() {
  return array(
    'CryptoCoin' => array(
      'label' => t('CryptoCoin'),
      'parent' => 'entity',
      'group' => t('CryptoCoin'),
    ),
    // Formatted text as used by in hook_entity_property_info() for text fields.
    'text_formatted' => array(
      'label' => t('formatted text'),
      'ui class' => 'RulesDataUITextFormatted',
      'wrap' => TRUE,
      'property info' => entity_property_text_formatted_info(),
    ),
  );
}

