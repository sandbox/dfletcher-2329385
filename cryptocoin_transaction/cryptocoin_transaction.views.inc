<?php

/**
 * Implements hook_views_data().
 */
function cryptocoin_transaction_views_data() {
  $data = array(
    'cryptocoin_transaction' => array(
      
      'table' => array (
        'title' => t('Transaction'),
        'group' => t('Cryptocoin'),
        'help' => t('Cryptocoin transactions.'),
        'base' => array(
          'field' => 'tid',
          'title' => t('Transaction'),
          'help' => t("Transactions record transfer of coin between users and deposits and withdrawls in and out of the system."),
          'weight' => -10,
        ),
      ),

      'coin_tradename' => array(
        'title' => t('Coin'),
        'help' => t('The trade name of the coin.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
          'numeric' => FALSE,
        ),
        'filter' => array(
          'handler' => ' views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),

      'account' => array(
        'title' => t('Account connected to this transaction.'),
        'help' => t('The transaction receiver, if local.'),
        'field' => array(
          'handler' => 'views_handler_field_user',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_user_uid',
          'numeric' => TRUE,
        ),
      ),

      'confirmations' => array(
        'title' => t('Confirmations'),
        'help' => t('Number of confirmations.'),
        'field' => array(
          'handler' => ' views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
        ),
        'filter' => array(
          'handler' => ' views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),

      'amount' => array(
        'title' => t('Amount'),
        'help' => t('Transaction amount.'),
        'field' => array(
          'handler' => ' views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
        ),
        'filter' => array(
          'handler' => ' views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),

      'address' => array(
        'title' => t('Address'),
        'help' => t('Transaction address.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
          'numeric' => FALSE,
        ),
        'filter' => array(
          'handler' => ' views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),

      'txid' => array(
        'title' => t('Transaction ID'),
        'help' => t('Transaction ID.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
          'numeric' => FALSE,
        ),
        'filter' => array(
          'handler' => ' views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),

    ),
  );
  return $data;
}
