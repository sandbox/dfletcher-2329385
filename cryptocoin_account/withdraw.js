(function ($) {

  if (typeof($.isNumeric) != 'function') {
    $.isNumeric = function(obj) {
      return !$.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
    }
  }

  function set_amount($form) {
    var $total = $form.find('.withdraw-total');
    var fee = parseFloat($form.find('INPUT[name="withdraw_fee"]').val());
    var amt = parseFloat($form.find('INPUT[name="amount"]').val());
    if (isNaN(amt)) {
      $total.hide();
      return;
    }
    var total = fee + amt;
    $total.find('.total').html(total);
    $total.show();
  }
  
  Drupal.behaviors.withdraw = {
    attach: function (context, settings) {

      var $withdraw_form = $('#cryptocoin-account-withdraw-form');
      $('#edit-amount').change(function() { set_amount($withdraw_form); });
      set_amount($withdraw_form);

      $('FORM.cryptocoin-withdraw-controls', context).submit(function(e) {

        e.preventDefault();

        var $form = $(this);

        var inputs_okay = true;
        var error_messages = [];

        // Validate the amount.
        var $amount = $form.find('INPUT[name="amount"]');
        $amount.removeClass('error');
        var amount = $amount.val().trim();
        if (!$.isNumeric(amount) || parseFloat(amount) <= 0.0) {
          $amount.addClass('error');
          error_messages[error_messages.length] = Drupal.t('Non-numeric amount value specified.');
          inputs_okay = false;
        }
        amount = parseFloat(amount);
        $amount.val(amount);

        // Validate the address.
        var $pubkey_address = $form.find('INPUT[name="pubkey_address"]');
        var pubkey_address = $pubkey_address.val().trim();
        var $address = $form.find('INPUT[name="address"]');
        $address.removeClass('error');
        var address = $address.val().trim();
        if (!validate_address_check(pubkey_address, address)) {
          $address.addClass('error');
          error_messages[error_messages.length] = Drupal.t('Invalid coin address.');
          inputs_okay = false;
        }
        $address.val(address);

        // Can't withdraw to deposit address.
        var $deposit_address = $form.find('INPUT[name="deposit_address"]');
        var deposit_address = $deposit_address.val();
        if (address == deposit_address) {
          $address.addClass('error');
          error_messages[error_messages.length] = Drupal.t('Cannot withdraw to deposit address.');
          inputs_okay = false;
        }

        // Check amount is above minimum.
        var $withdraw_minimum = $form.find('INPUT[name="withdraw_minimum"]');
        var withdraw_minimum = $withdraw_minimum.val().trim();
        if (amount < withdraw_minimum) {
          $amount.addClass('error');
          error_messages[error_messages.length] = Drupal.t('Withdraw amount is below minimum of !min.', {'!min': withdraw_minimum});
          inputs_okay = false;
        }

        // Validation passed?
        if (!inputs_okay) {
          console.log(JSON.stringify(error_messages));
          var $messagebox = $form.find('.message-box').empty();
          for (var i in error_messages) {
            var $message = $('<div>').addClass('messages').addClass('error').html(error_messages[i]).appendTo($messagebox);
          }
          return;
        }

        // Pop up the confirmation dialog.
        var $content = $('.cryptocoin-account-withdraw-confirm', this).clone();
        var buttons = { };
        buttons[Drupal.t('Withdraw')] = function() {
          var coin = $form.find('INPUT[name="coin_tradename"]').val();
          var account_name = $form.find('INPUT[name="account_name"]').val();
          var url = '/json/cryptocoin/account/withdraw/' + coin + '/' + account_name;
          var address = $form.find('INPUT[name="address"]').val();
          var amount = $form.find('INPUT[name="amount"]').val();
          var pubkey_address = $form.find('INPUT[name="pubkey_address"]').val();
          var form_token = $form.find('INPUT[name="form_token"]').val();
          var form_id = $form.find('INPUT[name="form_id"]').val();
          var form_build_id = $form.find('INPUT[name="form_build_id"]').val();
          var op = $form.find('INPUT[name="op"]').val();
          var data = { address: address, amount: amount, pubkey_address: pubkey_address, coin_tradename: coin, account_name: account_name, form_token: form_token, form_id: form_id, form_build_id: form_build_id, op: op };
          var $dialog = $(this);
          $dialog.dialog('option', 'dialogClass', 'json');
          $dialog.dialog('option', 'buttons', {
            'Close': function() {
              $(this).dialog('close');
            }
          });
          $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function(data, textStatus, jqXHR) {
              $dialog.dialog('option', 'dialogClass', 'with-close');
              $dialog.dialog('option', 'title', data.title);
              $dialog.dialog('option', 'closeOnEscape', true);
              $content.html(data.message);
              if (data.errors) {
                $dialog.dialog('option', 'dialogClass', 'withdraw-dialog-error');
                var errors = '<ul class="error-messages">';
                for (var i in data.errors) {
                  errors += '<li class="error-message">' + data.errors[i] + '</li>';
                }
                errors += "</ul>";
                $content.html(data.message + errors);
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {
              $dialog.dialog('option', 'dialogClass', 'with-close');
              $dialog.dialog('option', 'closeOnEscape', true);
              $dialog.dialog('option', 'title', textStatus);
              $content.html(Drupal.t('Error message given: !error', {'!error': errorThrown}));
            },
            dataType: 'json'
          });
        };
        buttons[Drupal.t('Cancel')] = function() {
          $(this).dialog('close');
        };
        $content.html($content.html().replace('AMOUNT', amount).replace('ADDRESS', address));
        $content.show().dialog({
          resizable: false,
          width: 450,
          height: 300,
          modal: true,
          closeOnEscape: false,
          title: Drupal.t('Withdraw confirmation'),
          dialogClass: 'no-close',
          buttons: buttons
        });
        return false;
      });
    }
  };

})(jQuery);
