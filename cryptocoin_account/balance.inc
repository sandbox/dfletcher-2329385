<?php

function cryptocoin_account_balance_json($coin, $account) {
  $balance = (isset($account->balance)) ? $account->balance / COIN : 0.0;
  drupal_json_output(array('data' => number_format($balance, 8)));
}

function cryptocoin_account_unconfirmed_json($coin, $account) {
  drupal_json_output(array('data' => (isset($account->unconfirmed) && ($account->unconfirmed > 0.0)) ? ('+ ' . number_format($account->unconfirmed / COIN, 8) . ' (' . $account->confirmations . ' of ' . $coin->confirmations . ')') : ''));
}

function template_process_cryptocoin_account_balance(&$vars) {

  // Load the account.
  $account = (isset($vars['balance']['#account'])) ? $vars['balance']['#account'] : NULL;
  $coin = (isset($vars['balance']['#coin'])) ? $vars['balance']['#coin'] : NULL;
  if ($coin && $account) {
    $balance = $account->balance / COIN;
    $unconfirmed = $account->unconfirmed / COIN;
    $confirmations = $account->confirmations;
  }
  else {
    $balance = '(';
    if (!$coin) {
      $balance .= 'no coin specified';
    }
    if (!$account) {
      if (!$coin) {
        $balance .= ', ';
      }
      $balance .= 'no account specified';
    }
    $balance .= ')';
    $unconfirmed = 0.0;
    $confirmations = 0;
  }

  // Returned content array.
  $content = array();
  $content['container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('account-balance'),
    ),
  );
  if (isset($vars['balance']['#label'])) {
    $content['container']['label'] = array(
      '#theme' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => array(
        'class' => array('label'),
      ),
      '#value' => $vars['balance']['#label'],
    );
  }
  $content['container']['balance'] = array(
    '#theme' => 'html_tag',
    '#tag' => 'span',
    '#attributes' => array(
      'class' => array('autodata', 'balance'),
      'data-auto' => url('json/cryptocoin/account/balance/' . $coin->tradename . '/' . $account->account_name),
    ),
    '#value' => number_format($balance, 8),
  );
  $content['container']['tradename'] = array(
    '#theme' => 'html_tag',
    '#tag' => 'span',
    '#attributes' => array(
      'class' => array('tradename'),
    ),
    '#value' => $coin->tradename,
  );

  $content['container']['unconfirmed'] = array(
    '#theme' => 'html_tag',
    '#tag' => 'span',
    '#attributes' => array(
      'class' => array('autodata', 'unconfirmed'),
      'data-auto' => url('json/cryptocoin/account/unconfirmed/' . $coin->tradename . '/' . $account->account_name),
    ),
    '#value' => ($unconfirmed > 0.0) ? ('+ ' . number_format($unconfirmed, 8) . ' (' . $confirmations . ' of ' . $coin->confirmations . ')') : '',
  );

  // If configured, wrap the balance data in a div that will receive a class
  // change notification that the data changed.
  $wrap = (isset($vars['balance']['#wrap'])) ? $vars['balance']['#wrap'] : TRUE;
  if ($wrap) {
    $content = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('autodata-notification-wrapper'),
      ),
      'content' => $content,
    );
  }

  // Attach necessary CSS and JavaScript.
  drupal_add_library('cryptocoin', 'autodata');
  $vars['render'] = $content;
}

function theme_cryptocoin_account_balance(&$vars) {
  return render($vars['render']);
}
