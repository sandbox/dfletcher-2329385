<?php

/**
 * Build the content array for the deposit form.
 */
function template_process_cryptocoin_account_deposit(&$vars) {
  $coin = & $vars['deposit']['#coin'];
  $vars['deposit']['render'][$coin->tradename . '-container'][$coin->tradename . '-deposit'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('deposit'),),
  );
  if ((!isset($vars['deposit']['#qr'])) || ($vars['deposit']['#qr'] == TRUE)) {
    $vars['deposit']['render'][$coin->tradename . '-container'][$coin->tradename . '-deposit']['qr'] = array(
      '#theme' => 'qr',
      '#data' => $vars['deposit']['#address'],
      '#classes' => array('deposit-qr'),
    );
  }
  $vars['deposit']['render'][$coin->tradename . '-container'][$coin->tradename . '-deposit']['address'] = array(
    '#theme' => 'container',
    '#attributes' => array(
      'class' => array('coin-address'),
    ),
    '#children' => $vars['deposit']['#address'],
  );
}

/**
 * Default theming function for cryptocoin_account_deposit.
 */
function theme_cryptocoin_account_deposit($vars) {
  return render($vars['deposit']['render']);
}
