<?php

/**
 * Ajax callback for the withdraw form.
 */
function cryptocoin_account_withdraw_json($coin, $account) {
  $form_state = array(
    'build_info' => array(
      'args' => array($coin, $account->account_name),
    ),
    'redirect' => 'json/cryptocoin/account/withdraw/success/' . $coin->tradename . '/' . $account->account_name . '/' . $_POST['amount'] . '/' . $_POST['address'],
  );
  $form = drupal_build_form('cryptocoin_account_withdraw_form', $form_state);
  $errors = form_get_errors();
  $success = ($errors || (!$form_state['submitted'])) ? FALSE : TRUE;
  $json = array('success' => $success);
  if ($success) {
    $json['title'] = t('Confirmed');
    if (variable_get('cryptocoin_account_withdraw_email_enabled', TRUE)) {
      $json['message'] = '<p>' . t('An email has been sent to approve withdraw of amount !amount !tradename to address !address. Please check your email to complete the withdraw.', array('!amount' => $form_state['values']['amount'], '!tradename' => $coin->tradename, '!address' => $form_state['values']['amount'])) . '</p>';
    }
    else {
      $json['message'] = '<p>' . t('Withdraw of amount !amount !tradename to address !address is confirmed.', array('!amount' => $form_state['values']['amount'], '!tradename' => $coin->tradename, '!address' => $form_state['values']['amount'])) . '</p>';
    }
  }
  else {
    $json['title'] = t('Error');
    $json['message'] = t('One or more errors occurred.');
    $json['errors'] = $errors;
    drupal_get_messages(); // clear message stack
  }
  drupal_json_output($json);
}

/**
 * Ajax callback for the withdraw form success.
 */
function cryptocoin_account_withdraw_json_success($coin, $account, $amount, $address) {
  $json = array(
    'title' => t('Confirmed'),
    'message' => '<p>' . t('Withdraw of amount !amount !tradename to address !address is confirmed.', array('!amount' => $amount, '!tradename' => $coin->tradename, '!address' => $address)) . '</p>',
  );
  if (variable_get('cryptocoin_account_withdraw_email_enabled', TRUE)) {
    $json['message'] = '<p>' . t('An email has been sent to approve withdraw of amount !amount !tradename to address !address. Please check your email to complete the withdraw.', array('!amount' => $amount, '!tradename' => $coin->tradename, '!address' => $address)) . '</p>';
  }
  else {
    $json['message'] = '<p>' . t('Withdraw of amount !amount !tradename to address !address is confirmed.', array('!amount' => $amount, '!tradename' => $coin->tradename, '!address' => $amount)) . '</p>';
  }
  drupal_json_output($json);
}

/**
 * Page builder for withdraw confirm/cancel link click.
 */
function cryptocoin_account_withdraw_user_confirm($action, $withdraw) {
  global $user;
  // weak query.
  $coin_tradename = db_select('cryptocoin_transaction', 't')->fields('t', array('coin_tradename'))->condition('tid', $withdraw->tid, '=')->execute()->fetchField();
  $coin = cryptocoin_load($coin_tradename);
  $tx = cryptocoin_transaction_load($coin, $withdraw->tid);
  $coin_account = cryptocoin_account_name_load($tx->account);
  if ($action == 'confirm') {
    $withdraw->user_confirmed = 1;
    $content = token_replace(_cryptocoin_account_withdraw_confirm_text(), array(
      'coin' => $coin,
      'account' => $coin_account,
      'tx' => $tx,
      'withdraw' => $withdraw,
    ));
    $tx->status = TXSTATUS_AWAITING_APP_APPROVAL; // Advance state.
  }
  else if ($action == 'cancel') {
    $withdraw->user_cancelled = 1;
    $content = token_replace(_cryptocoin_account_withdraw_cancel_text(), array(
      'coin' => $coin,
      'account' => $coin_account,
      'tx' => $tx,
      'withdraw' => $withdraw,
    ));
    $tx->status = TXSTATUS_CANCELED; // Transaction cancelled.
  }
  $withdraw->confirmation_user = $user->uid;
  $withdraw->confirmation_ip = ip_address();
  cryptocoin_account_withdraw_save($withdraw);
  cryptocoin_transaction_save($tx);
  return array(
    '#type' => 'markup',
    '#markup' => $content,
  );
}
