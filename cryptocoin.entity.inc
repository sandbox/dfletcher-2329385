<?php

/**
 * Extending the EntityAPIController for the Cryptocoin entity.
 */
class CryptocoinEntityController extends EntityAPIController {

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);
    // TODO additions to the $build render array
    $build['description'] = array(
      '#type' => 'markup',
      '#markup' => check_plain($entity->description),
      '#prefix' => '<div class="cryptocoin-description">',
      '#suffix' => '</div>',
    );
    return $build;
  }
  
}
